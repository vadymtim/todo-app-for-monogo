import React from 'react'
import {Link} from 'react-router-dom'

const AboutComponent:React.FC = () => {
    return(
        <div className="about-main">
            <h1 style={{textAlign: "center"}}>About</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ante quam, mattis posuere consequat sit amet, convallis ut turpis. Curabitur nisi mauris, tincidunt in posuere et, maximus nec quam. Vestibulum in odio vitae mauris tempor vulputate a a massa. Etiam non malesuada neque, imperdiet venenatis nulla. Mauris mauris eros, pulvinar condimentum finibus et, bibendum sit amet mauris. Proin non leo id sapien blandit consequat imperdiet et sapien. Proin laoreet ac lectus sed facilisis. Sed vestibulum sem sed risus commodo, ut auctor urna commodo. Quisque interdum augue mauris, id semper mauris efficitur non.</p>
            <p>Proin pretium risus hendrerit turpis auctor placerat. Maecenas erat justo, pharetra nec lacinia eu, posuere ac lacus. Aliquam quis posuere diam. Duis eget dui in leo pellentesque interdum sed ac risus. Proin sollicitudin dictum mauris, non dapibus ligula sagittis eu. Fusce imperdiet eu lorem quis tincidunt. Sed vel velit sollicitudin, gravida neque id, tempus elit. Integer et aliquet sem. Etiam consectetur sodales urna, vitae tristique est pulvinar id. Donec mauris ipsum, placerat non feugiat in, laoreet quis turpis. Fusce sit amet nunc nec nibh sollicitudin tincidunt sed a purus. Praesent interdum dolor at erat aliquam consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce non nisi ut dui interdum convallis quis sed tortor. Donec sodales dolor non sapien pharetra, et faucibus neque posuere.</p>
            <p>Duis eu turpis tincidunt, facilisis ante eu, egestas odio. Phasellus non facilisis nunc. In at risus vestibulum, efficitur elit vel, semper felis. Proin a viverra tortor, sed faucibus enim. Ut non mauris enim. Donec id dictum turpis, vitae venenatis enim. Donec feugiat ante in nulla interdum, a volutpat ipsum tristique. Cras mi odio, lobortis sit amet turpis eu, consequat suscipit odio. Ut sit amet nulla a magna malesuada sollicitudin quis et nulla.</p>
            <div style={{textAlign: "center"}}><Link to="/">Go to HomePage</Link></div>
        </div>
    );
}
export default AboutComponent;