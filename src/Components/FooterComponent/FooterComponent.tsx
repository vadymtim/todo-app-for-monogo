import React from 'react'
import imgLogo from '../../Images/monogo_logo.png'

const FooterComponent: React.FC = () => {
    return(
        <footer style={{padding: "30px 0", borderTop: "2px solid  rgb(221, 221, 221)", marginTop: "50px"}}>
            <div style={{textAlign: "center"}} className="header-container">
                <p>Made with <img width="20" alt="blue heart icon" src="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/apple/237/blue-heart_1f499.png"/> by <a href="mailto:vadym.tim@icloud.com">Vadym Tymoshenko</a></p>
                <p><strong>for</strong></p>
                <img width="200" src={imgLogo} alt="Monogo" />
            </div>
        </footer>
    );
}
export default FooterComponent;