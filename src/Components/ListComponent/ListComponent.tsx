import React, { useMemo } from 'react';
import doneImg from '../../Images/guarantee.svg';
import LoadingComponent from '../../Components/LoadingComponent/LoadingComponent'

type TData = {
    title: string,
    id: number,
    userId: number,
    completed: boolean
}
interface ITodoData {
    data: TData[],
    loading: boolean,
    todosPerPage: number,
    setTodosPerPage: React.Dispatch<React.SetStateAction<number>>
}
const ListComponent: React.FC<ITodoData> = ({ data, loading, todosPerPage, setTodosPerPage }) => {
    const perPageArr: number[] = [5, 7, 10, 15, 20]
    const perPageElements = (useMemo(() => perPageArr.map((page:number) => {
        return(
            <option key={page}>{ page }</option>
        )
    }), [perPageArr]))

    const listElements = useMemo(() => data.map((item: TData) => {
        let liStyles:string = "list-group-item li-element"
        if(item.id % 2 === 0) {
            liStyles += " background-li"
        }
        let doneImgStyle: string = "done-icon-main"
        if(item.completed) {
            doneImgStyle += " active-done-img"
        }
        return(
            <li className={liStyles} key={ item.id }>
                <div><strong>{item.id}.</strong> { item.title }</div>
                <div className={doneImgStyle}><img className="done-icon" alt="done icon" src={doneImg}/></div>
            </li>
        );
    }), [data])

    const changingTodosPerPage = (event: React.ChangeEvent<HTMLSelectElement>): void => {
        setTodosPerPage(parseInt(event.target.value))
    }
    if(loading) {
        return <LoadingComponent todosPerPage={todosPerPage} />
    }
    return(
        <div className="todo-list-main">
            <div className="info-before-list">
                <div className="done-info">
                    <img width={20} src={ doneImg } alt="Done Icon" />
                    <span> <strong>- Completed</strong></span>
                </div>
                <div className="form-group todos-per-page-main">
                    <label htmlFor="exampleFormControlSelect1"><strong>Todos per page</strong></label>
                    <select defaultValue={5} onChange={changingTodosPerPage} className="form-control" id="exampleFormControlSelect1">
                        { perPageElements }
                    </select>
                </div>
            </div>
            <ul className="list-group">
                { listElements }
            </ul>
        </div>
    );
}
export default ListComponent;