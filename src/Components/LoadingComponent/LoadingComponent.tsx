import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';

interface ILoadingProps {
    todosPerPage: number
}
const LoadingComponent: React.FC<ILoadingProps> = ({ todosPerPage }) => {
    const elements: object[] = [];
    for(let i = 0; i < todosPerPage; i++) {
        elements.push(
            <li key={i} className="list-group-item loading-li">
                <Skeleton variant="text" width="70%" />
                <Skeleton variant="circle" width={20} height={20} />
            </li>
        )
    }
    return(
        <div>
            <ul className="list-group ul-skeleton">
               {elements}
            </ul>
        </div>
    );
}
export default LoadingComponent;