import React, { useCallback } from 'react'
import { Link, NavLink } from 'react-router-dom'

interface IHeaderProps {
    setInputTerm: (item: string) => void;
}

const HeaderComponent: React.FC<IHeaderProps> = ({ setInputTerm }) => {
    const onInputChange = (useCallback(
        (event: React.ChangeEvent<HTMLInputElement>) => {
            setInputTerm(event.target.value)
        },
        [setInputTerm],
    ))
    return(
        <header>
            <div className="header-content header-container">
                <Link className="logo-link" to="/"><div className="logo-text">Todo<span className="logo-text-sub">S</span></div></Link>
                <ul className="menu-ul">
                    <li className="menu-li"><NavLink exact activeClassName="menu-li-active" to="/">Home</NavLink></li>
                    <li className="menu-li"><NavLink exact activeClassName="menu-li-active" to="/about">About</NavLink></li>
                </ul>
                <div className="search-input-main">
                    <input onChange={ onInputChange } className="form-control" placeholder="Search here..."/>
                </div>
            </div>
        </header>
    );
}
export default HeaderComponent;