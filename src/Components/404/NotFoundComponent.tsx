import React from 'react'
import { Link } from 'react-router-dom'

const NotFoundComponent: React.FC = () => {
    return(
        <div className="not-found-main">
            <h2 className="not-found-h2">Oooops... <strong><span className="not-found-h2-sub">Page not found!</span></strong></h2>
            <Link to="/">Go to HomePage</Link>
        </div>
    );
}

export default NotFoundComponent;