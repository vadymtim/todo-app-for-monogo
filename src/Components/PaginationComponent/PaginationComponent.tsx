import React, { useCallback, useMemo } from 'react';
import Pagination from '@material-ui/lab/Pagination';
import PaginationItem from '@material-ui/lab/PaginationItem';
import { Link } from 'react-router-dom'

interface IPaginationProps {
    setPageNumber: (item: number) => void,
    totalCount: number,
    todosPerPage: number,
    pageNumber: number
}
const PaginationComponent: React.FC<IPaginationProps> = ({setPageNumber, totalCount, todosPerPage, pageNumber}) => {

    const pageNumbers = (useMemo(() => 
        Math.ceil(totalCount / todosPerPage), 
    [totalCount, todosPerPage]));
    const handleChange = (useCallback(
        (event: React.ChangeEvent<unknown>, value: number) => {
            setPageNumber(value);
        },
        [setPageNumber],
    ))
    return(
        <div>
            <nav aria-label="Page navigation">
                <Pagination
                page={pageNumber}
                count={pageNumbers}
                onChange={handleChange}
                renderItem={(item) => (
                    <PaginationItem
                      component={Link}
                      to={`/page/${item.page}`}
                      {...item}
                    />
                  )}
                />
            </nav>
        </div>
    );
}
export default PaginationComponent;