import React, { useState, useEffect, useCallback } from 'react';
import axios from 'axios';
import './App.css';
import ListComponent from './Components/ListComponent/ListComponent'
import PaginationComponent from './Components/PaginationComponent/PaginationComponent';
import HeaderComponent from './Components/HeaderComponent/HeaderComponent';
import { BrowserRouter, Route, Switch }  from 'react-router-dom';
import NotFoundComponent from './Components/404/NotFoundComponent';
import AboutComponent from './Components/AboutComponent/AboutComponent';
import FooterComponent from './Components/FooterComponent/FooterComponent';

interface IData {
  userId: number,
  id: number,
  title: string,
  completed: boolean
}
const App: React.FC = (): JSX.Element => {
  const [data, setData] = useState<IData[]>([]);
  const [loading, setLoading] = useState(false);
  const [pageNumber, setPageNumber] = useState(1);
  const [todosPerPage, setTodosPerPage] = useState(5);
  const [inputTerm, setInputTerm] = useState('');

  //--- Fetching data ---//
  useEffect(() => {
    const fetchingData = async () => {
      setLoading(true);
      const res = await axios.get('https://jsonplaceholder.typicode.com/todos');
      setData(res.data);
      setTimeout(function lodingFuncState():void{
        setLoading(false);
      }, 700);
      
    }
    fetchingData();
  }, [])

  //--- Searching data ---//
  const searchData = (items: IData[], term: string) => {
    if(term.length === 0) {
      return (items);
    }
    return items.filter((item: IData) => {   
      return(item.title.toLowerCase().indexOf(term.toLowerCase()) > -1);
    })
  };
  const visibleItems = searchData(data, inputTerm);

  //--- Slice data ---//
  const todosLastIndex: number = pageNumber * todosPerPage;
  const todosFirstIndex: number = todosLastIndex - todosPerPage;
  const slicedData: IData[] = visibleItems.slice(todosFirstIndex, todosLastIndex);

  //--- Changing pageNumber for Route ---//
  const changingPageNumber = (useCallback(
    (id:number):void => {
      setPageNumber(id)
    },
    [setPageNumber],
  ))

  return (
    <div className="App">
      <BrowserRouter>
        <HeaderComponent setInputTerm={ setInputTerm }/>  
          <div className="main-container">
            <Switch>
              <Route path={["/page/:id", "/"]} exact>
                <Route path={["/page/:id", "/"]} exact>
                  <h1>Hey<img width="40" alt="hand icon" src="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/apple/237/waving-hand-sign_1f44b.png"/> This is TODO Application from <br/>Vadym Tymoshenko<img width="40" src="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/apple/237/man-raising-hand_1f64b-200d-2642-fe0f.png" alt="man icon"/></h1>
                </Route>
                <Route
                      exact
                      path={["/page/:id", "/"]}
                      render={({match}) => {
                        const {id} = match.params;
                        let pageId: number;
                        if(typeof(id) === "undefined") {
                          pageId = 1;
                        } else {
                          pageId = parseInt(id);
                        }
                        changingPageNumber(pageId)
                        return (<ListComponent setTodosPerPage={ setTodosPerPage } todosPerPage={todosPerPage} loading={ loading } data={slicedData} />)
                      }}
                />
                <Route exact path={["/page/:id", "/"]}>
                  <PaginationComponent pageNumber={ pageNumber }  setPageNumber={setPageNumber} totalCount={visibleItems.length} todosPerPage={todosPerPage} />
                </Route>
              </Route>
              <Route path="/about" component={AboutComponent} exact/>
              <Route component={NotFoundComponent} />
            </Switch>
          </div>
          <FooterComponent/>
      </BrowserRouter>
    </div>
  );
}
export default App;
